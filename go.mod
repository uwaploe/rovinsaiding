module bitbucket.org/uwaploe/rovinsaiding

go 1.15

require (
	bitbucket.org/uwaploe/go-focus v0.4.1
	bitbucket.org/uwaploe/go-svs v0.2.1
	bitbucket.org/uwaploe/ixblue v0.3.0
	github.com/nats-io/stan.go v0.8.3
	github.com/vmihailenco/msgpack v4.0.4+incompatible
)
